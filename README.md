# Portfolio

Il s'agit de mon projet personnel de portfolio en HTML/CSS. Il va me permettre d'exposer certains de mes projets

## Installation

Vous pouvez cloner le projet en utilisant git clone

```bash
git clone https://gitlab.com/mateusbubono/portfolio.git
```

## Usage

Vous pouvez ouvrir le portfolio en cliquant sur index.html directement après avoir cloné le projet ou simplement vous rendre à l'adresse suivante :

https://mateusbubono.gitlab.io/portfolio/